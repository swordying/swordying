## PHP MySQL 交互方式一：MySQLi

### 代码
```PHP
date_default_timezone_set("PRC");
// 数据库配置信息
$db_config=array(
    'DB_TYPE'=>'mysql',
    'DB_HOST'=>'127.0.0.1',
    'DB_USER'=>'root',
    'DB_PASSWORD'=>'root',
    'DB_NAME'=>'test',
    'DB_CHARSET'=>'utf8',
);
$table='user'; // 字段：id,name,password,status,addtime,updatetime
// 1、连接数据库
$mysqli = new mysqli($db_config['DB_HOST'],$db_config['DB_USER'],$db_config['DB_PASSWORD']);
if ($mysqli->connect_error){
    die("连接失败: " . $mysqli->connect_error);
}
// 2、选择数据库
$bool = $mysqli -> select_db($db_config['DB_NAME']);
// 3、定义字符集
$bool = $mysqli -> set_charset('utf8');
// ======= 增 ======= //
// 4、定义 SQL 语句
$current_time=time();
$user_name='user_'.date('YmdHis',$current_time);
$user_addtime=$current_time;
$user_updatetime=$current_time;
$sql="INSERT INTO {$table} (`name`,`addtime`,`updatetime`) VALUES ('{$user_name}',$user_addtime,$user_updatetime)";
// 5、执行 SQL 语句
$result=$mysqli->query($sql);
// 6、查看并处理结果集
if($result===true){
    echo '数据新增成功。id='.$mysqli->insert_id;
    echo "\n";
}
// ======= 删 ======= //
// 4、定义 SQL 语句
$sql="DELETE FROM {$table} WHERE id=3";
// 5、执行 SQL 语句
$result=$mysqli->query($sql);
$affected_rows=$mysqli -> affected_rows;
// 6、查看并处理结果集、即使删除条数为 0 也返回 true
if($affected_rows>0){
    echo '数据删除成功，影响条数='.$affected_rows;
    echo "\n";
}elseif($affected_rows===0){
    echo '没有数据被删除，影响条数='.$affected_rows;
    echo "\n";
}
// ======= 改 ======= //
// 4、定义 SQL 语句
$updatetime=time(); # 0
$sql = "UPDATE {$table} SET `updatetime`={$updatetime}";
// 5、执行 SQL 语句
$result=$mysqli->query($sql);
var_dump($result);
$affected_rows=$mysqli -> affected_rows;
// 6、查看并处理结果集、即使删除条数为 0 也返回 true
if($affected_rows>0){
    echo '数据更新成功，影响条数='.$affected_rows;
    echo "\n";
}elseif($affected_rows===0){
    echo '没有数据被更新，影响条数='.$affected_rows;
    echo "\n";
}
// ======= 查 ======= //
// 4、定义 SQL 语句
$sql = "SELECT * FROM {$table}";
// 5、执行 SQL 语句
$result = $mysqli -> query($sql);
// 6、查看并处理结果集
// 定义空数组、接收结果集
$num_rows = $result -> num_rows;
$rows = array();
if($result && $num_rows > 0) {
    while ($row = $result -> fetch_assoc()) {
        $rows[] = $row;
    }
}elseif($result && $num_rows === 0){
    echo '查询为空';
    echo "\n";
}else{
    echo '查询失败';
    echo "\n";
}
var_export($rows);
// 7、关闭连接
$mysqli->close();
```
### 总结：MySQLi 类操作数据库步骤
1. 连接 MySQL 服务器
2. 选择数据表
3. 设置字符集
4. 定义 SQL 语句
5. 执行 SQL 语句
6. 查看并处理结果集
7. 关闭 MySQL 连接
