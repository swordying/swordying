## 创建 PHP composer 组件

### 1. 初始化

- ` $ composer init `

#### 1. 声明包名
` Package name (<vendor>/<name>) `

#### 2. 声明描述
` Description [] `
- 可直接回车

#### 3. 声明作者信息
` Author [name <x@x.x>, n to skip]: `

#### 4. 声明组件接受的最低版本的依赖包
` Minimum Stability [] `
- 可直接回车

#### 5. 声明组件类型
` Package Type (e.g. library, project, metapackage, composer-plugin) [] `
- 可直接回车

#### 6. 声明许可证类型
` License []: `
- 可直接回车

#### 7. 定义组件的依赖关系
```bash
Define your dependencies.

Would you like to define your dependencies (require) interactively [yes]? 
```
- 是否以交互的方式定义依赖关系(yes)？ no 即可

#### 8. 定义组件的开发依赖关系
` Would you like to define your dev dependencies (require-dev) interactively [yes]? `
- 是否以交互的方式开发依赖关系(yes)？ no 即可

#### 9. 映射命名空间
` Add PSR-4 autoload mapping? Maps namespace "x\x" to the entered relative path. [src/, n to skip]: `
1. 添加 PSR-4 自动加载映射？映射命名空间 "x\x"
2. 直接回车亦可在 composer.json 中配置

#### 10. 确认初始化
` Do you confirm generation [yes]?  `
- 回车

#### 备注
1. 删除自动生成的 vendor 目录

### 2. composer.json 文件
```json
{
    "name": "x/x",
    "description": "描述",
    "type": "library",
    "license": "MIT",
    "autoload": {
        "psr-4": {
            "namespace\\": "src/"
        }
    },
    "authors": [
        {
            "name": "name",
            "email": "x@x.x"
        }
    ],
    "version": "1.0.0"
}
```
#### 1. autoload.autoload.psr-4 配置
1. ` namespace ` 与 src 目录中类文件中的命名空间一致，即可在自动加载类文件后使用命名空间引入类

### 3. GitHub 管理
- https://github.com/
1. 上传到版本管理平台，如 github
2. 更新代码，并提交
3. 添加 tag，并发布 releases，名称：` v1.0.0 `

### 4. packagist 管理
- https://packagist.org
1. submit 一个包
2. ` Repository URL (Git/Svn/Hg) ` 填写 clone 地址
