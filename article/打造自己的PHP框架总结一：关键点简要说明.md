## 打造自己的 PHP 框架总结一：关键点简要说明

### 框架名称：phpless
- Gitee 码云地址：https://gitee.com/swordying/phpless

1. 代码规范：可以尝试遵守 PSR 其中的相关规范，参考 http://psr.phphub.org 备注：链接已无效

2. URL 解析：关键点：获取 URL 中的传参 或者 ` $_SERVER['PATH_INFO'] ` 的值进行解析 示例：https://gitee.com/swordying/phpless/blob/master/phpless/core/Route.class.php

3. 自动加载：使用 spl_autoload_register 加载类文件，https://gitee.com/swordying/demo/tree/master/autoload

4. 信息配置：关键点：多次信息配置可以覆盖初始配置

5. URL 生成：生成符合 URL 解析要求的代码，示例 https://gitee.com/swordying/phpless/blob/master/phpless/Function/functions.php 374行 中的 url() 函数

- 备注：慕课网 https://www.imooc.com 可以找到打造自己的 PHP框架 相关课程

### 附录：PHP 框架选择观点
1. 选择第三方成熟的框架；
2. 利用 Composer组件 搭建的框架；
3. 打造一款自己的框架。
