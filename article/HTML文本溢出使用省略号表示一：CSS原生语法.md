## HTML 文本溢出使用省略号表示一：CSS 原生语法

### 1. 相关概念：
1. `white-space：nowrap;` 使元素内空白字符不换行，即自动忽略换行符，包括 Space[空格]键、Enter[回车]键、Tab[制表符]键形成的空白；
2. `text-overflow:ellipsis;` 规定当文本溢出包含元素时发生的事情，ellipsis 表示使用省略符号来代表溢出的文本；
3. `-webkit-：WebKit` 的 CSS 扩展属性；
4. `display:-webkit-box;` 必须结合的属性，将对象作为弹性伸缩盒子模型显示；
5. `-webkit-box-orient:vertical;` 必须结合的属性，设置或检索伸缩盒对象的子元素的排列方式；
6. `-webkit-line-clamp:2;` 用来限制在一个块元素显示的文本的行数。

### 2. 单行溢出
```HTML
<div style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;max-width:100px;">单行溢出效果，超过最大宽度，就会以省略号表示溢出文本</div>
```

### 3. 多行溢出
```HTML
<div style="display:-webkit-box;-webkit-box-orient:vertical;-webkit-line-clamp:2;overflow:hidden;line-height:20px;max-height:40px;max-width:200px;">
    多行溢出效果，超过最大高度，就会以省略号表示溢出文本
</div>
```

- 备注：此方法存在兼容性问题，但样式不会错乱。解决兼容性，请使用 jQuery.dotdotdot 插件或 Clamp.js 插件
