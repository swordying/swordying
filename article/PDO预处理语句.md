## PDO 预处理语句

### 简介
1. 概念：当需要执行多次 SQL 语句时，PDO 在服务器上创建查询过程，来对一类操作批量处理，优化程序；
2. 作用：节省开销、效率高、安全防 SQL 注入
3. 关键：PDO 中占位符 ` <?、:> ` 的运用

```PHP
// 数据库配置信息
$db_config=array(
    'DB_TYPE'=>'mysql',
    'DB_HOST'=>'127.0.0.1',
    'DB_USER'=>'root',
    'DB_PASSWORD'=>'root',
    'DB_NAME'=>'test',
    'DB_CHARSET'=>'utf8',
);
$table='user'; ## 字段：id,name,password,status,addtime,updatetime
// 1、定义 dsn
$dsn="{$db_config['DB_TYPE']}:host={$db_config['DB_HOST']};dbname={$db_config['DB_NAME']}";
// 2、调用 PDO 类
$pdo = new PDO($dsn,$db_config['DB_USER'],$db_config['DB_PASSWORD']);
// ======= 增 ======= //
// 3、定义 SQL 语句
// 3-1、使用占位符定义 SQL 语句
$sql = "INSERT INTO {$table} (`name`,`addtime`,`updatetime`)VALUES(:user_name,:addtime,:updatetime)";
// 3-2、生成 PDOStatement 对象，准备预处理语句
$pdo_statement = $pdo->prepare($sql);
// 4、执行 SQL 语句、返回值为受影响行数
// 4-1、绑定参数
// 4-1-1、bindParam 方法
$pdo_statement->bindParam(':user_name',$user_name);
$pdo_statement->bindParam(':addtime',$addtime);
$pdo_statement->bindParam(':updatetime',$updatetime);
// 插入数据
$current_time=time();
$user_name='user_'.date('YmdHis',$current_time);
$addtime=$current_time;
$updatetime=$current_time;
$result = $pdo_statement->execute();
// 5、查看并处理结果集
if($result){
    echo '插入成功，插入id='.$pdo->lastInsertId();
    echo "\n";
}else{
    echo "插入失败\n";
}
// 4-1-2、bindValue 方法
$pdo_statement->bindValue(':user_name',$user_name);
$pdo_statement->bindValue(':addtime',$addtime);
$pdo_statement->bindValue(':updatetime',$updatetime);
$result = $pdo_statement->execute();
// 5、查看并处理结果集
if($result){
    echo '插入成功，插入id='.$pdo->lastInsertId();
    echo "\n";
}else{
    echo "插入失败\n";
}
// ======= 删 ======= //
// 3、定义 SQL 语句
// 3-1、使用占位符定义 SQL 语句
$sql = "DELETE FROM {$table} WHERE id > :id limit 2";
// 3-2、生成 PDOStatement 对象，准备预处理语句
$pdo_statement = $pdo->prepare($sql);
// 4、执行 SQL 语句、生成 PDOStatement 对象，准备预处理语句
// 4-1、bindValue 方法
$pdo_statement->bindValue(':id',3);
// 4-2、执行 SQL 语句
$result = $pdo_statement->execute();
// 5、查看并处理结果集
if($result){
    echo '删除成功，删除数量='.$pdo_statement->rowCount();
    echo "\n";
}else{
    echo "删除失败\n";
}
// ======= 改 ======= //
// 3、定义 SQL 语句
// 3-1、使用占位符定义 SQL 语句
$sql = "UPDATE {$table} SET status=:status";
// 3-2、生成 PDOStatement 对象，准备预处理语句
// 4、执行 SQL 语句、生成 PDOStatement 对象，准备预处理语句
$pdo_statement = $pdo->prepare($sql);
// 4-1、bindValue 方法
$pdo_statement -> bindValue(':status',1);
// 4、执行 SQL 语句、返回值为受影响行数
// 4-2、执行 SQL 语句
$result = $pdo_statement->execute();
// 5、查看并处理结果集
if($result){
    echo '更新成功，更新数量='.$pdo_statement->rowCount();
    echo "\n";
}elseif($result===0){
    echo "没有更新\n";
}else{
    echo "更新失败\n";
}
// ======= 查 ======= //
// 3、定义 SQL 语句
// 3-1、使用占位符定义 SQL 语句
$sql = "SELECT * FROM {$table} WHERE id > :id ORDER BY id";
// 3-2、生成 PDOStatement 对象，准备预处理语句
$pdo_statement = $pdo->prepare($sql);
// 4、执行 SQL 语句、返回值为受影响行数
// 4-1、bindValue 方法
$pdo_statement -> bindValue(':id',3);
// 4-2、执行 SQL 语句
$result = $pdo_statement->execute();
// 5、查看并处理结果集
$row_count=$pdo_statement->rowCount();
$rows = array();
if($row_count){
    // 已索引数据获取查询结果
    while($row = $pdo_statement -> fetch(PDO::FETCH_ASSOC)){
        $rows[] = $row;
    }
    var_export($rows);
}elseif($row_count === 0){
    echo "查询为空\n";
}else{
    echo "查询失败\n";
}
// 6、关闭链接
$pdo=null;
```

### 总结
#### 1. PDO 预处理步骤
1. 设置 DSN；
2. 实例化 PDO；
3. 3-1、使用占位符声明 SQL 语句，
4. 3-2、通过 PDO 中的 prepare() 方法产生 PDOStatement 对象；
5. 4-1、绑定参数，
6. 4-2、执行 SQL 语句；
7. 查看并处理结果集；
8. 销毁对象、关闭数据库。
#### 2. 关键点：bindParam 与 bindValue 的区别
1. ` bindParam() ` 将 参数 绑定到相应的占位符上、先绑定后声明变量，
2. ` bindValue()` 将 值 绑定到相应的占位符上、先声明变量后绑定值。

