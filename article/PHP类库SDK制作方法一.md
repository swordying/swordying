## PHP 类库 SDK 制作方法一

- 以给定的 test 类库为例

### 1. 把 test 类库放入目录 Test 中
### 2. 新建文件 Test/TestSdk.php、Test/TestAutoloader.php
### 3. 编辑代码文件
#### 1. 编辑 TestSdk.php
```PHP
# 定义路径常量
define('TEST_SDK_PATH',dirname(__FILE__));
// 引入 TestAutoloader.php、注册相关类
require("TestAutoloader.php");
# 备注：未使用命名空间
```

#### 2. 编辑 TestAutoloader.php
```PHP
class TestAutoloader
{
    /**
     * 类库自动加载，写死路径，确保不加载其他文件
     * @param string $class 对象类名
     * @return void
     */
    public static function autoload($class)
    {
        $name = $class;
        if(false !== strpos($name,'\\')){
            $name = strstr($class, '\\', true);
        }
        $file_name = TEST_SDK_PATH."/test/{$name}.php";
        if(is_file($file_name)) {
            include $file_name;
            return;
        }
    }
}
# 备注：类文件的名称必须为类的名称
spl_autoload_register('TestAutoloader::autoload');
```

### 4. 在 PHP 代码中引入 TestSdk.php
