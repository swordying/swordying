### PHP 闭包与匿名回调函数的使用介绍

### 1、简介
1. 当使用次数很少时可以定义匿名函数；
2. 匿名函数可以封装一个代码段，但同时可以通过关键字 use 来附加变量；
3. 匿名函数可以对上下文的对象进行更改；
4. 匿名函数本质是对象，所以可以使用 $this 关键字；
5. 在 PHP 中闭包和匿名函数被视作相同的概念。

### 2、使用方法
#### 1. 基本使用
```php
$closure = function($param){
    return $param;
}; # 关键：不能省略半角英文分号
echo gettype($closure); # object
echo "\n";
echo $closure('实参'); # 实参
# 备注：在类方法中同样适用
```
#### 2. 关键字 use 的使用
```php
$extra = '附加变量';
$closure = function($param) use ($extra){
    return $param.$extra;
};
echo $closure('实参'); # 实参附加变量
# 备注：如果想改变 use 的附加变量，在附加变量前加 & 即可
```
#### 3、作为类方法的参数使用
```php
class Test
{
    public $name = 'test';
}
class Example
{
    public function setTest($test)
    {
        $this -> test = $test;
    }
    // 匿名函数作为类方法的参数使用
    public function useTest($closure)
    {
        $test = $this -> test;
        $closure($test);
    }
}
$example = new Example();
$example -> setTest(new Test());
// 使用匿名回调函数
$example -> useTest(function($param){
    $param -> name = 'testing';
});
echo $example -> test -> name; # testing
```
