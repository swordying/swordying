## PHP 抽奖代码

```PHP
## 声明：设定奖品、概率
$lucky_goods = array(
    1 => [
        'id' => 1,
        'probability' => 0,
        'prize' => '恭喜您抽中神秘奖品！',
    ],
    2 => [
        'id' => 2,
        'probability' => 0.01,
        'prize' => '微软（Microsoft）Xbox One S',
    ],
    3 => [
        'id' => 3,
        'probability' => 0.02,
        'prize' => '京东E卡经典卡1000面值（实体卡）',
    ],
    4 => [
        'id' => 4,
        'probability' => 0.03,
        'prize' => '罗技 机械键盘 K840',
    ],
    5 => [
        'id' => 5,
        'probability' => 0.94,
        'prize' => '祝您下次中奖！',
    ],
);
## 声明：样本随机量
$random_quantity = 100;
## 声明：随机奖品容器
$data = [];
// 判断所有奖品的获奖总概率是否为 1
$probability_sum = array_sum(array_map(function($v){
    return $v['probability'];
},$lucky_goods));
if(abs($probability_sum-1) > 0.0001){
    exit('所有奖品的总概率不为 1');
}
// 创建、随机奖品容器
foreach($lucky_goods as $k => $v){
    $count = $v['probability'] * $random_quantity;
    if($count > 0){
        for ($i = 1; $i <= $count; $i++) { 
            $data[] = $v['id'];
        }
    }
}
// 打乱、随机奖品容器
shuffle($data);
// 生成随机数
$lucky_key = mt_rand(0,$random_quantity - 1);
$lucky_goods_id = $data[$lucky_key];
$lucky_goods_data = $lucky_goods[$lucky_goods_id];
echo $lucky_key;
echo "\n";
echo $lucky_goods_id;
echo "\n";
var_export($lucky_goods_data);
```

- 备注：抽奖活动建议、最好服务器完成抽奖操作。

