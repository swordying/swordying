## PHP 下划线命名法与驼峰命名法互相转化

### 1. 代码
```php
class NamingTrans
{
    // 驼峰转下划线
    static function camel2underScore($camel = '')
    {
        if(empty($camel)){
            return '';
        }
        return strtolower(preg_replace('/((?<=[a-z])(?=[A-Z]))/','_',$camel));

    }
    // 下划线转驼峰 type：1=大驼峰，0=小驼峰，null=首字母原样输出
    static function underScore2camel($under_score = '',$type = null)
    {
        if(empty($under_score)){
            return '';
        }
        $first_word = $under_score[0];
        $separator = '_';
        $under_score = str_replace($separator, " ", strtolower($under_score));
        $result = str_replace(" ", "", ucwords($under_score));
        if($type === null){
            $result[0] = $first_word;
        }elseif($type === 0){
            $result[0] = strtolower($result[0]);
        }
        return $result;
    }
}
```

### 2. 示例
1. 驼峰转下划线：将 NamingTrans 转化为 naming_trans
```php
$result = NamingTrans::camel2underScore('NamingTrans');
var_export($result); # naming_trans
echo "\n";
```

2. 下划线转驼峰[首字母原样输出]：将 naming_trans 转化为 namingTrans
```php
$result = NamingTrans::underScore2camel('naming_trans');
var_export($result); # namingTrans
echo "\n";
```

3. 下划线转驼峰[大驼峰]：将 naming_trans 转化为 NamingTrans
```php
$result = NamingTrans::underScore2camel('naming_trans',1);
var_export($result); # NamingTrans
echo "\n";
```

4. 下划线转驼峰[小驼峰]：将 Naming_Trans 转化为 namingTrans
```php
$result = NamingTrans::underScore2camel('naming_trans',0);
var_export($result); # namingTrans
echo "\n";
```
