## HTML Table 表格滚动友好显示

### 1、简介
1. 问题：后台管理当表格 table 比较长时，内容显示不完整，若全部显示、则内容过于紧凑，若通过水平滚动查看、则关键信息不能保持
2. 需求：使长表格即能水平滚动，又保持关键信息
3. 思路：将表格分解为三个部分：左保持表格、中滚动表格、右保持表格，采用定位方法解决

### 2、关键点
1. 层叠样式 CSS 关键属性 ` position:relative|absolute ` 的使用
2. 中滚动表格宽度建议大于 1800px
3. 左中右的表格子标签高度必须保持一致，且宽度不宜过大

### 3、优化建议
1. 相关宽高的计算需要将所用前端框架的默认样式计算在内
2. 若想要布局的一致性，则可以将左中右表格通过 JavaScript 渲染
3. 表格子标签高度保持一致，有溢出省略与溢出滚动等方法

### 4、附录
1. 代码地址 [HTML Table 表格滚动友好显示](https://gitee.com/swordying/demo/blob/master/html-long-table-scrolling-display-friendly.html "HTML Table 表格滚动友好显示")
2. 此需求是被前端框架 Layui 所实现
3. 层叠样式 CSS 相关属性可以在慕课网学习，地址 [www.imooc.com/t/197450](https://www.imooc.com/t/197450 "精英讲师.张鑫旭")
