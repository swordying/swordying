## Web 开发之 MySQL 数据表应用一：关于网站基本配置数据的数据表创建问题

- 对于网站的 项目基本配置、网站基本信息、API 接口数据的储存，可以通过创建键值数据表来储存数据

### 具体如下：
- 公共字段有 addtime,updatetime,sort
#### 1. 网站配置：
1. 表名：web_config，
2. 字段：id,title,key,value,description；
#### 2. 网站信息：
1. 表名：web_info，
2. 字段：id,title,key,value,thumb,description；
#### 3. API 接口数据：
1. 表名：api_config,
2. 字段：id,title,key,value,description；

### 附录其他
1. 小技巧：在填写 key 时可以通过添加 字段前缀，定义一组相关的数据，这样就可以通过 MySQL LIKE 语法取出特定的一组数据，减少查询数据量；
2. 可以只配置一张键值表，通过 extra 字段来储存个性化数据。
