## PHP代码整洁之道一：优雅的编写工具类
- 以自定义分页类为例 代码地址：[https://gitee.com/swordying/demo/blob/master/paginator.class.php](https://gitee.com/swordying/demo/blob/master/paginator.class.php "自定义分页类")

### 1、相关概念
1. 自变量：变量传入对象后转化的属性
2. 因变量：因自变量改变而改变的属性
3. 其他变量：与自变量因变量相关的变量

## 2、注释格式
1. 类注释
    ```php
    /**
     * ------------------------------------
     * # 注释标题：类注释格式
     * ------------------------------------
     * 1. 采用 Markdown 格式
     * 2. 采用短横线上下分割相关内容
     * ------------------------------------
     */
    ```
2. 变量注释
    ```php
    # 变量用途
    ## 变量主体
    $variable = '使用 # 注释，同时体现一系列变量的层级关系';
    ```
3. 方法注释
    ```php
    // 方法使用 // 注释
    public function firstFunction(){}
    ```

### 3、传参规则
1. 传参的数量最多 2 个，如果是一个就直接传入，如果是多个就使用数组传入
2. 自变量如果是可以有默认值的则为非必传参数，其定义方法为在构造方法中声明以及设置默认值

### 4、方法规则
1. 构造方法中调用自变量的声明方法
2. 主体方法中调用因变量的声明方法

### 5、魔术方法调用属性规则
1. 自变量设置为公共变量，可以重新定义
2. 因变量设置为私有变量，不可以重新定义
3. 使用魔术方法，来获取但同时不修改因变量
    ```php
    // 禁止设置因变量，同时消除设置因变量出错的信息
    public function __set($key, $value)
    {
        return;
    }
    // 获取因变量
    public function __get($key)
    {
        return $this -> $key;
    }
    ```

### 附录其他
1. 根据文档编写的上传工具类 代码地址： [https://gitee.com/swordying/demo/blob/master/uploader.class.php](https://gitee.com/swordying/demo/blob/master/uploader.class.php "上传工具类")
