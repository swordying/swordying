## PHP 接口保持唯一会话方案一：利用默认 SESSION 机制

### 1. 关键代码
```PHP
$token=$_GET['token'];
if(empty($token)){
    session_start();
    $this->session_id=session_id();
}else{
    session_id($token);
    session_start();
    $this->session_id=$token;
}
```

### 2. 关键点

1. session 必须默认关闭；
2. session_id() 与 session_start() 相应的调用顺序不能改变；
3. 存在 token 时，调用 session_start() 会锁定 session_id，所以在此之前需要首先定义 session_id。

### 3. 知识点
1. session_start() 函数的使用；
2. session_id() 函数的使用。

### 附录其他
- 详细代码请点击 [API 较为详细代码](https://gitee.com/swordying/demo/blob/master/API接口基础类.php)
