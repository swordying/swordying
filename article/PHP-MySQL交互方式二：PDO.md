## PHP-MySQL 交互方式二：PDO

### PDO 简介
1. 概念PHP Date Object，即 PHP数据抽象层
2. 功能：可以屏蔽不用数据库之间的差异
3. 配置：php5 开始支持，打开配置文件，php.ini，启用：
    ```ini
    extension=php_pdo.dll  // 开启 PDO 
    extension=php_pdo_mysql.dll  // 使用 MySQL 
    ```

### 代码示例
```PHP
// 数据库配置信息
$db_config=array(
    'DB_TYPE'=>'mysql',
    'DB_HOST'=>'127.0.0.1',
    'DB_USER'=>'root',
    'DB_PASSWORD'=>'root',
    'DB_NAME'=>'test',
    'DB_CHARSET'=>'utf8',
);
$table='user'; // 字段：id,name,password,status,addtime,updatetime
// 1、定义 dsn
$dsn="{$db_config['DB_TYPE']}:host={$db_config['DB_HOST']};dbname={$db_config['DB_NAME']}";
// 2、调用 PDO 类
$pdo = new PDO($dsn,$db_config['DB_USER'],$db_config['DB_PASSWORD']);
// ======= 增 ======= //
$current_time=time();
$user_name='user_'.date('YmdHis',$current_time);
$addtime=$current_time;
$updatetime=$current_time;
// 3、定义 SQL 语句
$sql = "INSERT INTO {$table} (`name`,`addtime`,`updatetime`)VALUES('{$user_name}',{$addtime},{$updatetime})";
// 4、执行 SQL 语句、返回值为受影响行数
$result=$pdo->exec($sql);
// 5、查看并处理结果集
if($result){
    echo '插入成功，插入id='.$pdo->lastInsertId();
    echo "\n";
}else{
    echo "插入失败\n";
}
// ======= 删 ======= //
// 3、定义 SQL 语句
$sql = "DELETE FROM {$table} WHERE id>3 limit 2";
// 4、执行 SQL 语句、返回值为受影响行数
$result=$pdo->exec($sql);
// 5、查看并处理结果集
if($result){
    echo '删除成功，删除数量='.$result;
    echo "\n";
}else{
    echo "删除失败\n";
}
// ======= 改 ======= //
// 3、定义 SQL 语句
$sql = "UPDATE {$table} SET status=1";
// 4、执行 SQL 语句、返回值为受影响行数
$result=$pdo->exec($sql);
// 5、查看并处理结果集
if($result){
    echo '更新成功，更新数量='.$result;
    echo "\n";
}elseif($result===0){
    echo "没有更新\n";
}else{
    echo "更新失败\n";
}
// ======= 查 ======= //
// 3、定义 SQL 语句
$sql = "SELECT * FROM {$table}";
// 4、执行 SQL 语句
$pdo_statement = $pdo->query($sql);
// 5、查看并处理结果集
$row_count=$pdo_statement->rowCount();
$rows = array();
if($row_count){
    // 已索引数据获取查询结果
    while ($row = $pdo_statement -> fetch(PDO::FETCH_ASSOC)){
        $rows[] = $row;
    }
    var_export($rows);
}elseif($row_count === 0){
    echo "查询为空\n";
}else{
    echo "查询失败\n";
}
// 6、关闭链接
$pdo=null;
```

### 总结：关键之处是：DSN 的定义。
