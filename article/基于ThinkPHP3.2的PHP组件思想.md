## 基于 ThinkPHP 3.2 的 PHP 组件思想

- 后台表单处理的时候可以利用组件化思想，把常用的 表单DOM 利用 ThinkPHP 的 widget 控制器封装起来，调用起来就较为方便
- 表单DOM 包括但不限于：number、text、radio、checkbox、select、textarea

### 示例代码
```PHP
/**
 * --------------------------------
 * 组件：HTML 标签生成器
 * --------------------------------
 * 1. 后端基于 ThinkPHP 3.2
 * 2. 前端基于 BootStrap
 * 3. 作用：省略编写重复 HTML 代码的时间，加快后台开发
 * --------------------------------
 */
namespace Admin\Widget;
class HtmlWidget extends BaseWidget
{
    // 生成表单 input
    public function index($param)
    {
        $img = '';
        $label = $param['label'];
        $label_class = $param['label_class'] ?: 'col-sm-2';
        $name = $param['name'];
        $value = $param['value'];
        $input_type = $param['input_type']?:'text';
        $placeholder = $param['placeholder']?:'';
        $class = $param['class']?:'col-sm-4';
        $required = $param['required']?:'required';
        $disabled = $param['disabled']?:'';
        $title = $param['title']?:'';
        $input_class = $param['input_class']?:'form-control';
        $input_class = $param['input_class']?:'form-control';

        if(!$label)
            return "<div class='alert alert-danger' role='alert'>参数错误，请添加 label 标签！！！</div>";
        if(!$name)
            return "<div class='alert alert-danger' role='alert'>参数错误，请添加 name 标签！！！</div>";
        if($input_type == 'file' && $value){
            $required = '';
            $img="<img style='max-height:50px' src='{$value}' alt='{$label}' />";
        }
        $html=<<<EOF
<div class="form-group">
    <label for="label_{$name}" class="{$label_class} control-label">{$label}</label>
    <div class="{$class}">
        <input type="{$input_type}" title="{$title}" id="label_{$name}" name="{$name}" class="{$input_class}" placeholder="{$placeholder}" value="{$value}" {$required} {$disabled} />
        {$img}
    </div>
</div>
EOF;
        return $html;
    }
}
```

### 前端调用
```HTML
{:W('Html/index',array(['label'=>'input 组件：','name'=>'name'))}
```

- 总结：利用组件化思想，每个开发者都可以通过自己的一套后台框架 如 Bootstrap|AdminLTE，封装自己的组件，所以这里只是提供了示例的方案
